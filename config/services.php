<?php

return [
    'attendance'   =>  [
        'base_uri'  =>  env('ATTENDANCE_SERVICE_BASE_URL'),
        'secret'  =>  env('ATTENDANCE_SERVICE_SECRET'),
    ],
    'projects'   =>  [
        'base_uri'  =>  env('PROJECTS_SERVICE_BASE_URL'),
        'secret'  =>  env('PROJECTS_SERVICE_SECRET'),
    ],
    'users'   =>  [
        'base_uri'  =>  env('USERS_SERVICE_BASE_URL'),
    ],
];