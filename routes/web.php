<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});



// Matches "/login
$router->post('login', 'AuthController@login');

$router->group(['middleware' => 'auth.role:admin,user'], function () use ($router) {

    // Matches "/profile
    $router->get('profile', 'UserController@getProfile');

    $router->put('profile', 'UserController@updateProfile');

    $router->get('attendances', 'AttendanceController@getMyAttendence');

    $router->get('attendances/active', 'AttendanceController@getActiveAttendance');

    $router->post('attendances/start', 'AttendanceController@startAttendance');

    $router->post('attendances/end', 'AttendanceController@endAttendance');

    $router->put('attendances/{id}', 'AttendanceController@editAttendance');

    $router->delete('attendances/{id}', 'AttendanceController@deleteAttendance');

});

$router->group(['middleware' => 'auth.role:admin'], function () use ($router) {


    $router->get('attendances/{id}', 'AttendanceController@getAttendance');

    $router->delete('attendances/{id}/all', 'AttendanceController@deleteAllAttendance');

    $router->post('register', 'AuthController@register');

    $router->get('users/{id}', 'UserController@getUser');

    $router->delete('users/{id}', 'UserController@deleteUser');

    $router->get('users', 'UserController@allUsers');

    $router->get('attendance/{id}', 'AttendanceController@getAttendance');

    $router->put('users/{id}', 'UserController@updateUser');



    $router->get('projects', 'ProjectController@allProjects');

    $router->get('projects/{id}', 'ProjectController@getProject');

    $router->post('projects', 'ProjectController@addProject');

    $router->delete('projects/{id}', 'ProjectController@deleteProject');

    $router->put('projects/{id}', 'ProjectController@editProject');

    $router->get('projects/{id}/tasks', 'ProjectController@getTasks');



    $router->get('/tasks', 'ProjectController@getTasks');

    $router->post('/tasks', 'ProjectController@addTask');

    $router->get('/tasks/{id}', 'ProjectController@getTask');

    $router->post('/tasks/{id}/add-time', 'ProjectController@addTime');

    $router->delete('/tasks/{id}', 'ProjectController@deleteTask');

    $router->put('/tasks/{id}', 'ProjectController@editTask');
});