<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 26/11/2019
 * Time: 19:14
 */

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\ConsumeExternalService;

class Attendance
{
    use ConsumeExternalService;

    /**
     * The base uri to consume authors service
     * @var string
     */
    public $baseUri;

    /**
     * Authorization secret to pass to author api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.attendance.base_uri');
        $this->secret = config('services.attendance.secret');
    }


    /**
     * Obtain the full list of author from the author service
     */
    public function getActiveAttendance($id)
    {
        return $this->performRequest('GET', "/attendances/{$id}/active");
    }

    /**
     * Obtain user attendance
     */
    public function getAttendance($id)
    {
        return $this->performRequest('GET', "/attendances/{$id}");
    }

    public function startAttendance($id,Request $request)
    {

        return $this->performRequest('POST', "/attendances/{$id}/start",$request->all());
    }

    public function endAttendance($id,Request $request)
    {
        return $this->performRequest('POST', "/attendances/{$id}/end",$request->all());
    }

    /**
     * @param $id users id
     * @return string
     */
    public function deleteAllAttendance($id)
    {
        return $this->performRequest('DELETE', "/attendances/{$id}/all");
    }

    /**
     * @param $id attendance id
     * @return string
     */
    public function deleteAttendance($id)
    {
        return $this->performRequest('DELETE', "/attendances/{$id}");
    }


    public function editAttendance($id,Request $request)
    {
        return $this->performRequest('PUT', "/attendances/{$id}",$request->all());
    }

}