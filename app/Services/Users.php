<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 26/11/2019
 * Time: 19:14
 */

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use App\Traits\ConsumeExternalService;
use Laravel\Lumen\Http\Request;

class Users
{
    use ConsumeExternalService;

    /**
     * The base uri to consume authors service
     * @var string
     */
    public $baseUri;

    /**
     * Authorization secret to pass to author api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.users.base_uri');
        $this->secret = config('services.users.secret');
    }

    public function login($request)
    {
        return $this->performRequest('POST', "/oauth/token", $request);
    }

    public function allUsers()
    {
        return $this->performRequest('GET', "/api/users/", array(), [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . Auth::user()->access_token]);
    }

    /**
     * Get the authenticated User.
     * @var string
     *
     * @return string
     */
    public function getProfile($access_token)
    {
        return $this->performRequest('GET', "/api/users/profile", array(), [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $access_token]);
    }

    public function getUser($id)
    {
        return $this->performRequest('GET', "/api/users/{$id}", array(), [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . Auth::user()->access_token]);
    }

    public function updateUser($id, $request)
    {
        return $this->performRequest('PUT', "/api/users/{$id}", $request, [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . Auth::user()->access_token]);

    }

    public function changePassword($request)
    {
        return $this->performRequest('PUT', "/api/users/password", $request, [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . Auth::user()->access_token]);

    }

    public function deleteUser($id)
    {
        return $this->performRequest('DELETE', "/api/users/{$id}", array(), [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . Auth::user()->access_token]);
    }



}