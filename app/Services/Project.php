<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 26/11/2019
 * Time: 19:14
 */

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Traits\ConsumeExternalService;

class Project
{
    use ConsumeExternalService;

    /**
     * The base uri to consume authors service
     * @var string
     */
    public $baseUri;

    /**
     * Authorization secret to pass to author api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.projects.base_uri');
        $this->secret = config('services.projects.secret');
    }




    /**
     * Obtain user project
     */
    public function getProject($id)
    {
        return $this->performRequest('GET', "/projects/{$id}");
    }

    public function addProject($data)
    {
        return $this->performRequest('POST', "/projects", $data);
    }

    public function getProjects()
    {
        return $this->performRequest('GET', "/projects");
    }

    public function editProject($data, $id)
    {
        return $this->performRequest('PUT', "/projects/{$id}", $data);
    }

    public function deleteProject( $id)
    {
        return $this->performRequest('DELETE', "/projects/{$id}");
    }

    public function getProjectTasks($id)
    {
        return $this->performRequest('GET', "/projects/{$id}/tasks");
    }


    /**
     * Obtain user task
     */
    public function getTask($id)
    {
        return $this->performRequest('GET', "/tasks/{$id}");
    }

    public function addTask($data)
    {
        return $this->performRequest('POST', "/tasks", $data);
    }

    public function addTime($data, $id)
    {
        return $this->performRequest('POST', "/tasks/{$id}/add-time",$data);
    }

    public function getTasks()
    {
        return $this->performRequest('GET', "/tasks");
    }

    public function editTask($data, $id)
    {
        return $this->performRequest('PUT', "/tasks/{$id}", $data);
    }

    public function deleteTask( $id)
    {
        return $this->performRequest('DELETE', "/tasks/{$id}");
    }

}