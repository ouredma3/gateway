<?php

namespace App\Http\Controllers;

use App\Services\Users;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Auth;
use  App\User;
use Illuminate\Http\Request;

class UserController
{
    use ApiResponser;

    public $usersService;

    public function __construct(Users $usersService)
    {
        $this->usersService = $usersService;
    }

    /**
     * Get the authenticated User.
     *
     * @return Response
     */
    public function getProfile()
    {
        return $this->successResponse($this->usersService->getProfile());
    }

    /**
     * Get all Users.
     *
     * @return Response
     */
    public function allUsers()
    {
        return $this->successResponse($this->usersService->allUsers());
    }

    /**
     * Get user by id.
     *
     * @return Response
     */
    public function getUser($id)
    {
        try {
            return $this->successResponse($this->usersService->getUser($id));
        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }

    }

    /**
     * Update authenticated user data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(Request $request)
    {


        try {
            $id = Auth::user()->user_id;
            return $this->successResponse($this->usersService->updateUser($id, $request));

        } catch (\Exception $e) {
            return response()->json(['message' => 'user not found!'], 404);
        }

    }

    /**
     * Update User data by id
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(Request $request, $id)
    {
        try {
            return $this->successResponse($this->usersService->updateUser($id, $request));

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }

    }

    /**
     * Delete user by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser($id)
    {
        try {
            return $this->successResponse($this->usersService->deleteUser($id));

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }
    }

}