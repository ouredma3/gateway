<?php

namespace App\Http\Controllers;

use App\Services\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use  App\User;

class AuthController extends Controller
{

    public $usersService;

    public function __construct(Users $usersService)
    {
        $this->usersService = $usersService;
    }

    /**
     * Store a new user.
     *
     * @param  Request $request
     * @return Response
     */
    public function register(Request $request)
    {
        //validate incoming request
        $this->validate($request, [
            'name' => 'required|string',
            'role' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        try {

            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->role = $request->input('role');
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);

            $user->save();

            //return successful response
            return response()->json(['user' => $user, 'message' => 'CREATED'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!'], 409);
        }

    }

    /**
     * Get a JWT via given credentials.
     *
     * @param  Request $request
     * @return Response
     */
    public function login(Request $request)
    {

        $login_data = array('username' => $request->input('username'), 'password' => $request->input('password'), 'client_id' => 2, 'client_secret' => 'n5Jy3v5F5QVQZrZLTW6irvOuYmJYthplGqQH2sF4', 'grant_type' => 'password', 'scope' => '*');

        try {
            $userToken = json_decode($this->usersService->login($login_data));
        } catch (\Exception $e) {
            return response()->json(['message' => 'Invalid credentials'], 401);
        }

        try {
            $userData = json_decode($this->usersService->getProfile( $userToken->access_token));
        } catch (\Exception $e) {
            return response()->json(['message' => 'Application Error'], 500);
        }
        $user = User::where(['email' => $userData->email])->first();

        if ($user) {
            $user->access_token = $userToken->access_token;
            $user->role = $userData->role;
        } else {
            $user = User::create([
                'user_id' => $userData->id,
                'email' => $userData->email,
                'access_token' => $userToken->access_token,
                'role' => $userData->role,
            ]);
        }


        if (!$token = Auth::login($user)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }


}