<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 26/11/2019
 * Time: 18:53
 */

namespace App\Http\Controllers;

use App\Services\Project;
use  App\User;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ProjectController
{
    use ApiResponser;

    public $projectService;

    public function __construct(Project $projectService)
    {
        $this->projectService = $projectService;
    }

    public function allProjects()
    {
        return $this->successResponse($this->projectService->getProjects());
    }

    public function addProject(Request $request)
    {
        return $this->successResponse($this->projectService->addProject($request->all()));
    }

    public function getProject($id)
    {

        return $this->successResponse($this->projectService->getProject($id));
    }

    public function getTasks($id)
    {
        return $this->successResponse($this->projectService->getProjectTasks($id));
    }

    public function editProject(Request $request, $id)
    {
        return $this->successResponse($this->projectService->editProject($request->all(), $id));

    }

    public function deleteProject($id)
    {
        try {
            return $this->successResponse($this->projectService->deleteProject($id));

        } catch (\Exception $e) {

            return response()->json(['error' => $e->getMessage()], 400);
        }

    }

    public function addTask(Request $request)
    {
        try {
            return $this->successResponse($this->projectService->addTask($request->all()));
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function getTask($id)
    {
        try {
            return $this->successResponse($this->projectService->getTask($id));
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function editTask(Request $request, $id)
    {
        try {
            return $this->successResponse($this->projectService->editTask($request->all(), $id));
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

    }

    public function addTime(Request $request,$id)
    {
        try {
            return $this->successResponse($this->projectService->addTime($request->all(),$id));
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function deleteTask($id)
    {
        try {
            return $this->successResponse($this->projectService->deleteTask($id));
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

    }
}