<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 26/11/2019
 * Time: 18:53
 */

namespace App\Http\Controllers;

use App\Services\Attendance;
use  App\User;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AttendanceController
{
    use ApiResponser;

    public $attendanceService;

    public function __construct(Attendance $attendanceService)
    {
        $this->attendanceService = $attendanceService;
    }

    public function getMyAttendence()
    {
        $id = Auth::user()->user_id;
        return $this->successResponse($this->attendanceService->getAttendance($id));
    }

    public function getAttendance($id)
    {
        try {
            return $this->successResponse($this->attendanceService->getAttendance($id));
        } catch (\Exception $e) {
            return response()->json(['message' => 'user not found!'], 404);
        }

    }

    public function startAttendance(Request $request)
    {
        $id = Auth::user()->user_id;
        return $this->successResponse($this->attendanceService->startAttendance($id,$request));
    }

    public function getActiveAttendance()
    {
        $id = Auth::user()->user_id;
        return $this->successResponse($this->attendanceService->getActiveAttendance($id));
    }

    public function endAttendance(Request $request)
    {
        $id = Auth::user()->user_id;
        return $this->successResponse($this->attendanceService->endAttendance($id,$request));
    }

    public function editAttendance($id,Request $request)
    {
        return $this->successResponse($this->attendanceService->editAttendance($id,$request));
    }

    public function deleteAttendance($id)
    {
        return $this->successResponse($this->attendanceService->deleteAttendance($id));
    }

    public function deleteAllAttendance($id)
    {
        return $this->successResponse($this->attendanceService->deleteAllAttendance($id));
    }
}